FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > musl.log'

COPY musl .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' musl
RUN bash ./docker.sh

RUN rm --force --recursive musl
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD musl
